﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace wps_simple
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
            slImages.addImage("1.jpg");
            slImages.addImage("2.jpg");
            slImages.addImage("3.jpg");
            imgView.Source = new BitmapImage(new Uri(slImages.imgList[index], UriKind.Relative));
        }

        private int index = 0;
        private List<Image> images;
        private slideImages slImages = new slideImages();

        private void Next_Click(object sender, RoutedEventArgs e)
        {
            if (index < slImages.imgList.Count - 1)
            {
                index += 1;
                imgView.Source = new BitmapImage(new Uri(slImages.imgList[index], UriKind.Relative));
            }
        }

        private void Prev_Click(object sender, RoutedEventArgs e)
        {
            if (index > 0)
            {
                index -= 1;
                imgView.Source = new BitmapImage(new Uri(slImages.imgList[index], UriKind.Relative));
            }
        }

        private void handleShutdown_click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }

    class slideImages
    {
        private string url;
        public List<string> imgList = new List<string>();

        public void addImage(string imgName)
        {
            imgList.Add("Images/" + imgName);
        }

    }
}
